﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using MyOutlookTimeTrackingExtension.Helper;

namespace MyOutlookTimeTrackingExtension.Tests
{
    [TestClass()]
    public class TimeHelperTests
    {
        readonly DateTime _monday;
        readonly DateTime _tuesday;
        readonly DateTime _wednesday;
        readonly DateTime _thursday;
        readonly DateTime _friday;
        readonly DateTime _saturday;
        readonly DateTime _sunday;
        readonly List<DateTime> _week;

        public TimeHelperTests()
        {
            _monday = new DateTime(2016, 12, 5);
            _tuesday = _monday.AddDays(1);
            _wednesday = _monday.AddDays(2);
            _thursday = _monday.AddDays(3);
            _friday = _monday.AddDays(4);
            _saturday = _monday.AddDays(5);
            _sunday = _monday.AddDays(6);

            _week = new List<DateTime>()
            {
                _monday,
                _tuesday,
                _wednesday,
                _thursday,
                _friday,
                _saturday,
                _sunday
            };
        }

        [TestMethod()]
        public void GetEndOfWeekTest()
        {
            var timeOfDay = _saturday.AddSeconds(-1).TimeOfDay;

            foreach (var day in _week)
            {
                var endOfweek = TimeHelper.GetEndOfWeek(day);
                Assert.AreEqual(_friday.Day, endOfweek.Day, $"Wrong Day for {day.DayOfWeek}");
                Assert.AreEqual(timeOfDay, endOfweek.TimeOfDay, $"Wrong TimeOfDay for {day.DayOfWeek}");
            }
        }

        //[TestMethod()]
        //public void TimeHelperTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetStartOfWeekTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetDaysAfterMondayTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetDaysAfterFridayTest()
        //{
        //    Assert.Fail();
        //}
    }
}