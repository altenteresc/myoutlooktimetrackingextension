using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Helper
{
    public static class TimeHelper
    {
        public static DateTime GetSelectedWeeksEndDateOrNow(Application application)
        {
            var endOfWeek = DateTime.Now;
            var activeExplorer = application.ActiveExplorer();
            var currentView = activeExplorer.CurrentView;
            if (currentView is CalendarView)
            {
                var calendarView = currentView as CalendarView;
                if (calendarView.CalendarViewMode == OlCalendarViewMode.olCalendarViewWeek ||
                    calendarView.CalendarViewMode == OlCalendarViewMode.olCalendarView5DayWeek)
                {
                    var dates = GetDisplayedDatesList(calendarView);
                    var lastDate = dates.Last();
                    if (lastDate < endOfWeek)
                    {
                        endOfWeek = GetEndOfWeek(lastDate);
                    }
                }
            }

            ComHelper.Release(currentView);
            ComHelper.Release(activeExplorer);

            return endOfWeek;
        }

        public static List<DateTime> GetDisplayedDatesList(CalendarView calendarView)
        {
            var list = new List<DateTime>();
            foreach (var displayedDate in calendarView.DisplayedDates)
            {
                list.Add((DateTime)displayedDate);
            }
            return list;
        }

        public static DateTime GetStartOfWeek(DateTime date)
        {
            var startOfWeek = date.AddDays(-GetDaysAfterMonday(date)).Date;
            return new DateTime(startOfWeek.Year, startOfWeek.Month, startOfWeek.Day, 0, 0, 0, DateTimeKind.Local);
        }

        public static int GetDaysAfterMonday(DateTime now)
        {
            int daysAfterMonday = 0;
            while (now.AddDays(-daysAfterMonday).DayOfWeek != DayOfWeek.Monday)
            {
                daysAfterMonday++;
            }
            return daysAfterMonday;
        }

        public static int GetDaysAfterFriday(DateTime now)
        {
            //WTF??? gehts noch komplizierter?!
            int daysAfterFriday = 0;
            while (now.AddDays(-daysAfterFriday).DayOfWeek != DayOfWeek.Friday)
            {
                daysAfterFriday++;
                if (daysAfterFriday > 2)
                {
                    daysAfterFriday = -5;
                }
            }
            return daysAfterFriday;
        }

        public static DateTime GetEndOfWeek(DateTime date)
        {
            //WTF???
            var endOfWeek = date.AddDays(-GetDaysAfterFriday(date)).Date;
            return new DateTime(endOfWeek.Year, endOfWeek.Month, endOfWeek.Day, 23, 59, 59, DateTimeKind.Local);
        }
    }
}