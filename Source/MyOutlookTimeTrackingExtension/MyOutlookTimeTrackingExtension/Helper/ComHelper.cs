﻿using System.Runtime.InteropServices;

namespace MyOutlookTimeTrackingExtension.Helper
{
    public static class ComHelper
    {
        public static void Release(object obj)
        {
            if (obj != null)
            {
                Marshal.ReleaseComObject(obj);
            }
        }
    }
}
