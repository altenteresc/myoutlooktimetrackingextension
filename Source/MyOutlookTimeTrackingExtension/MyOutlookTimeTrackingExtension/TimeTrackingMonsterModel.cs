using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Office.Interop.Outlook;
using MyOutlookTimeTrackingExtension.Helper;
using MyOutlookTimeTrackingExtension.Properties;
using static System.String;
using Application = Microsoft.Office.Interop.Outlook.Application;

namespace MyOutlookTimeTrackingExtension
{
    public class TimeTrackingMonsterModel
    {
        internal Application _application;

        public TimeTrackingMonsterModel(Application application)
        {
            _application = application ?? new Application();
        }

        public TimeSpan GetWeeklyWorkingTime(DateTime start, DateTime end)
        {
            return GetWeeklyWorkingTime(start, end, GetExcludedItemsKeyWords());
        }

        public TimeSpan GetWeeklyWorkingTime(DateTime start, DateTime end, List<String> excludeList)
        {
            var foundItems = GetCalendarItems(start, end, excludeList);

            var result = GetTotalTimeOfAppointmentItems(foundItems, new RelevantItemFilter(true, false));

            foreach (var foundItem in foundItems)
            {
                ComHelper.Release(foundItem);
            }

            return result;
        }

        public TimeSpan GetTotalHoursOfSelectedAppointmentItems()
        {
            var foundItems = GetSelectedCalendarItems();

            var result = GetTotalTimeOfAppointmentItems(foundItems, new RelevantItemFilter(true, false));

            foreach (var foundItem in foundItems)
            {
                ComHelper.Release(foundItem);
            }

            return result;
        }

        public List<AppointmentItem> GetCalendarItems(DateTime start, DateTime end)
        {
            return GetCalendarItems(start, end, null);
        }

        public List<AppointmentItem> GetCalendarItems(DateTime start, DateTime end, List<String> excludeList)
        {
            var calendar = GetWorkingHoursFolder();

            var calendarItems = calendar.Items;
            calendarItems.Sort("[Start]", Type.Missing);
            calendarItems.IncludeRecurrences = true;

            var filter = CreateFilter(start, end);
            var foundItems = calendarItems.Restrict(filter);

            var items = ToAppointmentItemList(foundItems);

            items = ApplyExcludeFilter(excludeList, items);

            return items;
        }

        private static List<AppointmentItem> ApplyExcludeFilter(List<string> excludeList, List<AppointmentItem> items)
        {
            if (excludeList != null && excludeList.Count > 0)
            {
                Func<AppointmentItem, bool> predicate =
                    item => item != null &&
                        !excludeList.Exists(
                            excludedWord =>
                                (item.Subject != null && item.Subject.Contains(excludedWord)) || 
                                (item.Categories != null && item.Categories.Contains(excludedWord)) || 
                                (item.Body != null && item.Body.Contains(excludedWord)));

                items = items.Where(predicate).ToList();
            }
            return items;
        }

        public AppointmentItem GetLastAppointmentItem(DateTime start, DateTime end)
        {
            var items = GetCalendarItems(start, end);
            return items.Last();
        }

        public AppointmentItemActionResult AddAppointmentItem(string message, string category,
            DateTime start, DateTime end)
        {
            var result = new AppointmentItemActionResult();

            AppointmentItem item = CreateAppointmentItem();

            if (item != null)
            {
                item.Start = start;
                item.End = end;
                item.AllDayEvent = false;
                item.Subject = message;
                item.Categories = category;
                item.Save();
                //item.Display(false);
                //item.Close(OlInspectorClose.olSave);
                result.ActionResult = ActionResult.Add;
            }

            return result;
        }

        public AppointmentItemActionResult UpdateAppointmentItem(AppointmentItem item, string message, string category,
            DateTime start, DateTime end)
        {
            var result = new AppointmentItemActionResult();

            if (item != null)
            {
                item.Start = start;
                item.End = end;
                item.AllDayEvent = false;
                item.Subject = message;
                item.Categories = category;
                item.Save();
                //item.Display(false);
                //item.Close(OlInspectorClose.olSave);
                result.ActionResult = ActionResult.Update;
            }

            return result;
        }

        public AppointmentItemActionResult UpdateAppointmentItemEndTime(AppointmentItem item, DateTime end)
        {
            var result = new AppointmentItemActionResult();

            if (item != null)
            {
                var start = item.Start;
                var now = DateTime.Now;

                if (start < now)
                {
                    item.End = now;
                    item.AllDayEvent = false;
                    item.Save();
                    result.ActionResult = ActionResult.Update;
                }
            }

            return result;
        }

        public AppointmentItem CreateAppointmentItem()
        {
            var calendar = GetWorkingHoursFolder();
            return calendar.Items.Add(OlItemType.olAppointmentItem);
        }

        public MAPIFolder GetWorkingHoursFolder()
        {
            var calendar = GetCalendar(Settings.Default.WorkingHoursCalendarName);
            return calendar;
        }

        private MAPIFolder GetCalendar(string calendarName)
        {
            var mapiNamespace = _application.GetNamespace("MAPI");
            var calendars = mapiNamespace.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);

            var session = _application.Session;
            var folders = calendars.Folders;
            var mapiFolders = folders.Cast<MAPIFolder>();
            var calendar = mapiFolders.FirstOrDefault(folder => folder.Name == calendarName) ??
                           session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);
            return calendar;
        }

        private static string CreateFilter(DateTime start, DateTime end)
        {
            var startString = start.Date.ToString("g");
            var nowString = end.ToString("g");
            var filter = $"[Start] >= '{startString}' AND ([End] <= '{nowString}' OR [Start] < '{nowString}')";
            
            return filter;
        }

        //private static string AddExcludeFilters(string filter, List<string> excludeList)
        //{
        //    filter = excludeList.Aggregate(filter, (current, exclude) =>
        //        string.Format("{0} AND @SQL{1}http://schemas.microsoft.com/mapi/proptag/0x0037001f{1} ci_phrasematch {1}{2}{1}", 
        //        current, (char) 34, exclude));
        //        //$" AND @SQL='urn:schemas:contacts:sn' ci_phrasematch '{exclude}'");
        //    return filter;
        //}

        public List<AppointmentItem> GetSelectedCalendarItems()
        {
            var activeExplorer = _application.ActiveExplorer();
            var selection = activeExplorer.Selection;

            var result = ToAppointmentItemList(selection);

            ComHelper.Release(selection);
            ComHelper.Release(activeExplorer);

            return result;
        }

        private static TimeSpan GetTotalTimeOfAppointmentItems(Items foundItems, RelevantItemFilter relevantItemFilter)
        {
            var workingHours = TimeSpan.Zero;

            if (foundItems != null && foundItems.Count > 0)
            {
                foreach (AppointmentItem appointmentItem in foundItems)
                {
                    if (IsRelevantAppointmentItem(appointmentItem, relevantItemFilter))
                    {
                        var timespan = appointmentItem.End - appointmentItem.Start;
                        workingHours += timespan;
                    }
                }
            }

            return workingHours;
        }

        private static TimeSpan GetTotalTimeOfAppointmentItems(ICollection<AppointmentItem> foundItems,
            RelevantItemFilter relevantItemFilter)
        {
            var workingHours = TimeSpan.Zero;

            if (foundItems != null && foundItems.Count > 0)
            {
                foreach (AppointmentItem appointmentItem in foundItems)
                {
                    if (IsRelevantAppointmentItem(appointmentItem, relevantItemFilter))
                    {
                        var timespan = GetDurationOfAppointmentItem(appointmentItem);
                        workingHours += timespan;
                    }
                }
            }

            return workingHours;
        }

        private static TimeSpan GetDurationOfAppointmentItem(AppointmentItem appointmentItem)
        {
            var factor = GetWorkingHourFactor(appointmentItem);

            var duration = appointmentItem.End - appointmentItem.Start;
            return TimeSpan.FromSeconds((duration.TotalSeconds) * factor);
        }

        private static double GetWorkingHourFactor(AppointmentItem appointmentItem)
        {
            double factor = 1d;
            if (appointmentItem != null && appointmentItem.Body != null && appointmentItem.Body.Length > 0)
            {
                try
                {
                    string body = appointmentItem.Body.ToLowerInvariant();
                    var validnumber = Double.TryParse(body, NumberStyles.Float, CultureInfo.CurrentCulture, out factor);
                    if (!validnumber)
                    {
                        if (body.Contains("factor"))
                        {
                            var index = body.IndexOf("factor");
                            var textFromFactor = body.Substring(index, body.Length);
                            var newLineIndex = body.IndexOf(Environment.NewLine);
                            var hasNewLine = newLineIndex > 0;
                            var factorText = textFromFactor.Substring(0, hasNewLine ? newLineIndex : textFromFactor.Length);
                            var parts = factorText.Split(':');
                            if (parts.Length > 1)
                            {
                                var factorValueString = parts[1];
                                validnumber = Double.TryParse(parts[1], NumberStyles.Float, CultureInfo.CurrentCulture, out factor);
                            }
                        }
                    }
                    if (!validnumber)
                    {
                        factor = 1d;
                    }
                }
                catch
                {
                    factor = 1d;
                }
            }
            return factor;
        }

        private static bool IsRelevantAppointmentItem(AppointmentItem appointmentItem, RelevantItemFilter filter)
        {
            return (!filter.IgnoreAllDayEvents || !appointmentItem.AllDayEvent) &&
                   (!filter.MustHaveSubject || !IsNullOrEmpty(appointmentItem.Subject));
        }

        public struct RelevantItemFilter
        {
            public bool IgnoreAllDayEvents { get; }

            public bool MustHaveSubject { get; }

            public RelevantItemFilter(bool ignoreAllDayEvents, bool mustHaveSubject)
            {
                MustHaveSubject = mustHaveSubject;
                IgnoreAllDayEvents = ignoreAllDayEvents;
            }
        }

        private static List<AppointmentItem> ToAppointmentItemList(Items foundItems)
        {
            var items = new List<AppointmentItem>();
            foreach (var item in foundItems)
            {
                if (item is AppointmentItem)
                {
                    items.Add(item as AppointmentItem);
                }
            }
            return items;
        }

        private static List<AppointmentItem> ToAppointmentItemList(Selection selection)
        {
            var items = new List<AppointmentItem>();
            for (int i = 1; i <= selection.Count; i++)
            {
                var item = selection[i];
                if (item is AppointmentItem)
                {
                    items.Add(item as AppointmentItem);
                }
            }

            return items;
        }

        public List<Category> GetRecentCategories(int daysInPast, int topCounts)
        {
            Categories categories = _application.Session.Categories;

            var allCategories = new Dictionary<string, Category>();
            var categoryCounter = new Dictionary<string, int>();

            foreach (Category category in categories)
            {
                if (!allCategories.ContainsKey(category.CategoryID))
                {
                    allCategories.Add(category.CategoryID, category);
                    categoryCounter.Add(category.CategoryID, 0);
                }
            }

            var start = DateTime.Now.AddDays(-daysInPast).Date;
            var items = GetCalendarItems(start, DateTime.Now);
            
            foreach (AppointmentItem item in items)
            {
                var key = item.Categories;
                if (!IsNullOrEmpty(key) && categoryCounter.ContainsKey(key))
                {
                    categoryCounter[key] = categoryCounter[key] + 1;
                }
            }

            var itemsWithCount =
                categoryCounter.Select(item => new KeyValuePair<Category, int>(allCategories[item.Key], item.Value))
                    .ToList();

            var take = itemsWithCount.OrderByDescending(left => left.Value).Take(topCounts).ToList();

            return take.Select(item => item.Key).ToList();
        }

        private List<string> GetExcludedItemsKeyWords()
        {
            var keyWords = Settings.Default.ExcludedItemsKeyWords;
            if (IsNullOrWhiteSpace(keyWords))
            {
                return null;
            }
            var strings = keyWords.Split(',');
            return strings.ToList();
        }

    }

    public class AppointmentItemActionResult: BaseActionResult<AppointmentItem>
    {
    }

    public class BaseActionResult<T>
    {
        public T Item { get; set; }
        public ActionResult ActionResult;
    }

    public enum ActionResult
    {
        Nothing,
        Add,
        Update,
        Delete
    }
}