﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOutlookTimeTrackingExtension.Controls.Other;

namespace MyOutlookTimeTrackingExtension
{
    partial class WindowFormRegionCollection
    {
        internal TimeTrackingFormRegion TimeTrackingFormRegion
        {
            get
            {
                foreach (var item in this)
                {
                    if (item.GetType() == typeof(TimeTrackingFormRegion))
                        return (TimeTrackingFormRegion)item;
                }
                return null;
            }
        }
    }
}
