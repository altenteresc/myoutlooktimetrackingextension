﻿using System;
using Microsoft.Office.Interop.Outlook;
using MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane;
using Application = Microsoft.Office.Interop.Outlook.Application;

namespace MyOutlookTimeTrackingExtension
{
    public partial class ThisAddIn
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Tools.Office.ProgrammingModel.dll", "14.0.0.0")]
        internal Application Application;

        private void ThisAddIn_Startup(object sender, EventArgs e)
        {
            Application.Inspectors.NewInspector += InspectorsOnNewInspector;

            //var timeTracking = new TimeTrackingTaskPane();
            var timeTracking = new DynamicTaskPane();

            var customTaskPane = CustomTaskPanes.Add(timeTracking, "MOTTE");
            customTaskPane.Visible = true;
        }
        
        private void InspectorsOnNewInspector(Inspector inspector)
        {
            
        }

        private void ThisAddIn_Shutdown(object sender, EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see http://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Startup += ThisAddIn_Startup;
            Shutdown += ThisAddIn_Shutdown;
        }

        #endregion
    }
}