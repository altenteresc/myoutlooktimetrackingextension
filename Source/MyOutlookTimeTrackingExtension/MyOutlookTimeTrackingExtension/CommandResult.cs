using System;

namespace MyOutlookTimeTrackingExtension
{
    public class CommandResult<T>
    {
        public T Value { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }

        public CommandResult()
        {
            
        }

        public CommandResult(T value, string message, Exception exception)
        {
            Value = value;
            Message = message;
            Exception = exception;
        }

        public static CommandResult<T> Ok(T value, string message)
        {
            return new CommandResult<T>(value, message, null);
        }

        public static CommandResult<T> Ok(string message)
        {
            return new CommandResult<T>(default(T), message, null);
        }

        public static CommandResult<T> Error(string message, Exception ex)
        {
            return new CommandResult<T>(default(T), message, ex);
        }
    }
}