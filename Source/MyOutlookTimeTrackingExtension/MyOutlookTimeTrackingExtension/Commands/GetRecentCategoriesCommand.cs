﻿using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;
using MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls;

namespace MyOutlookTimeTrackingExtension.Commands
{
    class GetRecentCategoriesCommand : CommandBase<GetRecentCategoryCommandArgs, List<Category>>
    {
        public GetRecentCategoriesCommand(GetRecentCategoryCommandArgs arguments) : base(arguments)
        {
        }

        protected override CommandResult<List<Category>> InnerRun()
        {
            var result = Model.GetRecentCategories(Arguments.DaysInPast, Arguments.TakeTopCount);
            return new CommandResult<List<Category>>(result, "Success", null);
        }
    }

    public class GetRecentCategoryCommandArgs : CommandArgument
    {
        public int DaysInPast { get; set; }
        public int TakeTopCount { get; set; }

        public GetRecentCategoryCommandArgs(Application application, int daysInPast, int topCount) : base(application)
        {
            DaysInPast = daysInPast;
            TakeTopCount = topCount;
        }

    }
}
