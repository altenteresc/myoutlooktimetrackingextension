using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    internal class UpdateLastWorkingItemEndTimeCommand : CommandBase<UpdateLastWorkingItemEndTimeCommandArguments, List<AppointmentItemActionResult>>
    {
        public UpdateLastWorkingItemEndTimeCommand(UpdateLastWorkingItemEndTimeCommandArguments args) : 
            base(args)
        {
        }

        protected override CommandResult<List<AppointmentItemActionResult>> InnerRun()
        {
            var results = new List<AppointmentItemActionResult>();
            try
            {
                var start = DateTime.Today;
                var end = DateTime.Now;
                var lastAppointmentItem = Model.GetLastAppointmentItem(start, end);
                
                if (lastAppointmentItem != null)
                {
                    var endOfLastTask = DateTime.Now;
                    var result1 = Model.UpdateAppointmentItemEndTime(lastAppointmentItem, endOfLastTask);
                    results.Add(result1);
                }
            }
            catch (System.Exception ex)
            {
                return new CommandResult<List<AppointmentItemActionResult>>(null, "Error", ex);
            }

            return new CommandResult<List<AppointmentItemActionResult>>(results, "Success", null);
        }
    }

    public class UpdateLastWorkingItemEndTimeCommandArguments : CommandArgument
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public UpdateLastWorkingItemEndTimeCommandArguments(Application application, DateTime startTime, DateTime endTime) : base(application)
        {
            StartTime = startTime;
            EndTime = endTime;
        }
    }
}