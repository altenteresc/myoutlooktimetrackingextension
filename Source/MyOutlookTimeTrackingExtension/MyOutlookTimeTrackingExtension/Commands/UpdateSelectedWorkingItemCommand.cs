using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    internal class UpdateSelectedWorkingItemCommand : CommandBase<UpdateSelectedWorkingItemCommandArguments, List<AppointmentItemActionResult>>
    {
        public UpdateSelectedWorkingItemCommand(UpdateSelectedWorkingItemCommandArguments args) : 
            base(args)
        {
        }

        protected override CommandResult<List<AppointmentItemActionResult>> InnerRun()
        {
            var results = new List<AppointmentItemActionResult>();
            
            var subject = Arguments.Subject;
            var category = Arguments.Category;

            var newSubjectIsEmpty = string.IsNullOrEmpty(subject);
            var newCategoryIsEmpty = string.IsNullOrEmpty(category);
            
            var items = Arguments.AppointmentItems;

            // spec:
            // if we have an appointment item selected do update text and category if specified
            
            foreach (AppointmentItem item in items)
            {
                var itemSubject = item.Subject;
                var itemCategory = item.Categories;

                itemSubject = newSubjectIsEmpty ? itemSubject : subject;
                itemCategory = newCategoryIsEmpty ? itemCategory : category;

                var result = Model.UpdateAppointmentItem(item, itemSubject, itemCategory, item.Start, item.End);
                results.Add(result);
            }

            return new CommandResult<List<AppointmentItemActionResult>>(results, "Success", null);
        }
    }

    public class UpdateSelectedWorkingItemCommandArguments : CommandArgument
    {
        public List<AppointmentItem> AppointmentItems { get; set; }
        public string Subject { get; set; }
        public string Category { get; set; }

        public UpdateSelectedWorkingItemCommandArguments(Application application, List<AppointmentItem> appointmentItems, string subject, string category) : base(application)
        {
            AppointmentItems = appointmentItems;
            Subject = subject;
            Category = category;
        }
    }
}