﻿using System;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    class GetLastWorkingItemCommand : CommandBase<GetLastWorkingItemCommandArgs, AppointmentItem>
    {
        public GetLastWorkingItemCommand(GetLastWorkingItemCommandArgs arguments) : base(arguments)
        {
        }

        protected override CommandResult<AppointmentItem> InnerRun()
        {
            var result = Model.GetLastAppointmentItem(DateTime.Today, DateTime.Now);

            return new CommandResult<AppointmentItem>(result, "Success", null);
        }
    }

    public class GetLastWorkingItemCommandArgs : CommandArgument
    {
        public GetLastWorkingItemCommandArgs(Application application) : base(application)
        {
        }
    }
}
