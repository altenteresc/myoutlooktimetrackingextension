using Microsoft.Office.Interop.Outlook;
using Exception = System.Exception;

namespace MyOutlookTimeTrackingExtension.Commands
{
    //TODO run commands in an executor
    //TODO maybe TResult should inherit base class CommandResult
    //TODO Error result should be handled in special sub class of CommandResult
    public abstract class CommandBase<TArg, TResult> where TArg : CommandArgument
    {
        protected readonly TArg Arguments;
        protected TimeTrackingMonsterModel Model;

        protected CommandBase(TArg arguments)
        {
            Arguments = arguments;
        }

        public CommandResult<TResult> Run()
        {
            CommandResult<TResult> result;

            try
            {
                Model = new TimeTrackingMonsterModel(Arguments.Application);

                result = InnerRun();
            }
            catch (Exception ex)
            {
                result = CommandResult<TResult>.Error(@"Error!", ex);
            }

            return result;
        }

        protected abstract CommandResult<TResult> InnerRun();

    }

    public class CommandArgument
    {
        public Application Application { get; set; }

        public CommandArgument(Application application)
        {
            Application = application;
        }
    }
}