using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    internal class UpdateLastWorkingItemCommand : CommandBase<UpdateLastWorkingItemCommandArguments, List<AppointmentItemActionResult>>
    {
        public UpdateLastWorkingItemCommand(UpdateLastWorkingItemCommandArguments args) : 
            base(args)
        {
        }

        protected override CommandResult<List<AppointmentItemActionResult>> InnerRun()
        {
            var results = new List<AppointmentItemActionResult>();
            try
            {

                var subject = Arguments.Subject;
                var category = Arguments.Category;

                var newSubjectIsEmpty = string.IsNullOrEmpty(subject);
                var newCategoryIsEmpty = string.IsNullOrEmpty(category);

                var start = DateTime.Today;
                var end = DateTime.Now;
                var lastAppointmentItem = Model.GetLastAppointmentItem(start, end);

                //spec:
                //get last item to update text and category if specified and more importantly update the END date of the item
                //close (update end date) if the content is the same (subject, category)
                //close old and create new item if content is different
                //if we have no category selected and no subject then do nothing
                //if there is not last item today create a new one

                if (lastAppointmentItem != null)
                {
                    var createItem = false;
                    var itemSubject = lastAppointmentItem.Subject;
                    var itemCategory = lastAppointmentItem.Categories;
                    
                    var itemHasSubject = !string.IsNullOrEmpty(itemSubject);
                    var hasSameSubject = string.Equals(itemSubject, subject, StringComparison.InvariantCultureIgnoreCase);

                    var itemHasCategory = !string.IsNullOrEmpty(itemCategory);
                    var hasSameCategory = string.Equals(itemCategory, category, StringComparison.InvariantCultureIgnoreCase);

                    var endOfLastTask = DateTime.Now;

                    // decision tree:
                    if (itemHasSubject && !newSubjectIsEmpty && !hasSameSubject)
                    {
                        createItem = true;
                    }
                    else if (itemHasSubject && !hasSameSubject && !newCategoryIsEmpty && hasSameCategory)
                    {
                        createItem = true;
                    }
                    else if (!newCategoryIsEmpty && hasSameCategory && !hasSameSubject)
                    {
                        createItem = true;
                    }
                    else if (itemHasSubject && !newCategoryIsEmpty)
                    {
                    }

                    //TODO make decision tree easier:
                    if (itemHasSubject)
                    {
                        if (itemHasCategory)
                        {

                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (itemHasCategory)
                        {

                        }
                        else
                        {

                        }
                    }


                    if (!createItem)
                    {
                        itemSubject = newSubjectIsEmpty || hasSameSubject ? itemSubject : subject;
                        itemCategory = newCategoryIsEmpty ? itemCategory: category;
                    }
                    
                    var result1 = Model.UpdateAppointmentItem(lastAppointmentItem, itemSubject, itemCategory, lastAppointmentItem.Start, endOfLastTask);
                    results.Add(result1);

                    if (createItem && result1.ActionResult == ActionResult.Update)
                    {
                        //create new item if the last one is closed
                        var startOfNewTask = DateTime.Now;
                        var endOfNewTask = startOfNewTask.AddMinutes(1);
                        var result2 = Model.AddAppointmentItem(subject, category, startOfNewTask, endOfNewTask);
                        results.Add(result2);
                    }
                }
                else
                {
                    var startOfNewTask = DateTime.Now;
                    var endOfNewTask = startOfNewTask.AddMinutes(15);
                    var result = Model.AddAppointmentItem(subject, category, startOfNewTask, endOfNewTask);
                    results.Add(result);
                }
            }
            catch (System.Exception ex)
            {
                return new CommandResult<List<AppointmentItemActionResult>>(null, "Error", ex);
            }

            return new CommandResult<List<AppointmentItemActionResult>>(results, "Success", null);
        }
    }

    public class UpdateLastWorkingItemCommandArguments : CommandArgument
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Subject { get; set; }
        public string Category { get; set; }

        public UpdateLastWorkingItemCommandArguments(Application application, DateTime startTime, DateTime endTime, string subject, string category) : base(application)
        {
            StartTime = startTime;
            EndTime = endTime;
            Subject = subject;
            Category = category;
        }
    }
}