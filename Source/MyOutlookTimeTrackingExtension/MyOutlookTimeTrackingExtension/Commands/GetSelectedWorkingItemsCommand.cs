﻿using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    class GetSelectedWorkingItemsCommand : CommandBase<GetSelectedWorkingItemsCommandArgs, List<AppointmentItem>>
    {
        public GetSelectedWorkingItemsCommand(GetSelectedWorkingItemsCommandArgs arguments) : base(arguments)
        {
        }

        protected override CommandResult<List<AppointmentItem>> InnerRun()
        {
            var result = Model.GetSelectedCalendarItems();

            return new CommandResult<List<AppointmentItem>>(result, "Success", null);
        }
    }

    public class GetSelectedWorkingItemsCommandArgs : CommandArgument
    {
        public GetSelectedWorkingItemsCommandArgs(Application application) : base(application)
        {
        }
    }
}
