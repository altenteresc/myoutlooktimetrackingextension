using System;
using Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Commands
{
    internal class GetTotalHoursOfSelectedApointmentItemsCommand : CommandBase<CommandArgument, TimeSpan>
    {
        public GetTotalHoursOfSelectedApointmentItemsCommand(CommandArgument argument) : base(argument)
        {
        }

        protected override CommandResult<TimeSpan> InnerRun()
        {
            var time = Model.GetTotalHoursOfSelectedAppointmentItems();

            return CommandResult<TimeSpan>.Ok(time, time.ToString());
        }
    }
}