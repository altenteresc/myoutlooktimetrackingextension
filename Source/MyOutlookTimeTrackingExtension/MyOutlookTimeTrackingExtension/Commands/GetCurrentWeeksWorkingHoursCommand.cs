using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;
using MyOutlookTimeTrackingExtension.Helper;

namespace MyOutlookTimeTrackingExtension.Commands
{
    public class GetCurrentWeeksWorkingHoursCommand : CommandBase<CurrentWeekArguments, TimeSpan>
    {
        public GetCurrentWeeksWorkingHoursCommand(CurrentWeekArguments args) : 
            base(args)
        {
        }

        protected override CommandResult<TimeSpan> InnerRun()
        {
            var endDate = Arguments.EndDate;
            if (endDate == DateTime.MinValue)
            {
                endDate = TimeHelper.GetSelectedWeeksEndDateOrNow(Arguments.Application);
            }
            var startOfWeek = TimeHelper.GetStartOfWeek(endDate);

            var time = Model.GetWeeklyWorkingTime(startOfWeek, endDate);
            return CommandResult<TimeSpan>.Ok(time, time.ToString());
        }
    }

    public class CurrentWeekArguments : CommandArgument
    {
        public DateTime EndDate { get; set; }

        public CurrentWeekArguments(Application application, DateTime endDate) : base(application)
        {
            EndDate = endDate;
        }
    }
}