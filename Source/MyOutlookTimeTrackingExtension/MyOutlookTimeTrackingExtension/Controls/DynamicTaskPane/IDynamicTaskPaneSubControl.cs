using System;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane
{
    public interface IDynamicTaskPaneSubControl
    {
        string SubControlName { get; }
    }
}