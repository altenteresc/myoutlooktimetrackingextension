﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane
{
    public partial class DynamicTaskPane : UserControl
    {
        public DynamicTaskPane()
        {
            InitializeComponent();

            //CreateSubControlsDynamically();
            CreateSubControlsManually();
        }

        private void CreateSubControlsManually()
        {
            var weeklyHoursButton = new WeeklyWorkingHoursButton();
            addSubControl(weeklyHoursButton);

            var selectedHoursButton = new SelectedItemsWorkingHoursButton();
            addSubControl(selectedHoursButton);

            //var calendatItemBuzzer = new DynamicCalendarItemsBuzzer();
            //flowLayoutPanel.Controls.Add(calendatItemBuzzer);

            var updateEndTimeButton = new UpdateLastWorkingItemButton();
            addSubControl(updateEndTimeButton);
        }

        private void addSubControl(DynamicTaskPaneSubControlBase subControl)
        {
            flowLayoutPanel.Controls.Add(subControl);
        }

        private void CreateSubControlsDynamically()
        {
            //use this when there is a configuration of active subcontrols implemented
            //use Settings file or MAPIFolder.GetStorage through model
            var foundTypes = GetAllRelevantTypes();

            foreach (var type in foundTypes)
            {
                var control = Activator.CreateInstance(type);
                var userControl = (UserControl) control;
                flowLayoutPanel.Controls.Add(userControl);
            }
        }

        private List<Type> GetAllRelevantTypes()
        {
            var typeInfos = Assembly.GetAssembly(GetType()).DefinedTypes.ToList();
            return typeInfos.Where(type => type.ImplementedInterfaces.Contains(typeof (IDynamicTaskPaneSubControl)) &&
                                           type.BaseType == typeof (UserControl)).Cast<Type>().ToList();
        }

        private void DynamicTaskPane_Load(object sender, EventArgs e)
        {
        }
    }
}
