﻿using System;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Commands;
using Exception = System.Exception;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public partial class DynamicCalendarItemsBuzzer : DynamicTaskPaneSubControlBase
    {
        public DynamicCalendarItemsBuzzer()
        {
            InitializeComponent();
        }

        private void InitializeListView()
        {
            var args = new GetRecentCategoryCommandArgs(Globals.ThisAddIn.Application, 30, 5);
            var cmd = new GetRecentCategoriesCommand(args);
            var result = cmd.Run();

            if (result.Exception == null && result.Value != null && result.Value.Count > 0)
            {
                var categories = result.Value;
                foreach (var category in categories)
                {
                    var item = new ListViewItem
                    {
                        Text = category.Name,
                        Name = category.Name
                    };
                    //var color = ColorTranslator.FromOle((int)category.Color);
                    //var baseColor = Color.FromArgb(color.R, color.G, color.B);
                    //item.BackColor = Color.FromArgb(128, baseColor);

                    lv_Categories.Items.Add(item);
                }
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            var application = Globals.ThisAddIn.Application;

            // spec: 
            // get selected appointment item for update
            // if no item is selected then the last will be used

            var selectedItemsCmdArgs = new GetSelectedWorkingItemsCommandArgs(application);
            var selectedItemsCmd = new GetSelectedWorkingItemsCommand(selectedItemsCmdArgs);
            var selectedItemsResult = selectedItemsCmd.Run();

            Exception ex = null;
            var message = string.Empty;

            if (selectedItemsResult.Exception == null && selectedItemsResult.Value != null &&
                selectedItemsResult.Value.Count > 0)
            {
                var items = lv_Categories.SelectedItems;
                var category = items.Count > 0 ? items[0].Text : "";
                var subject = tb_Message.Text;

                var args = new UpdateSelectedWorkingItemCommandArguments(
                    application, selectedItemsResult.Value, subject, category);
                var updateLastWorkingItemCommand = new UpdateSelectedWorkingItemCommand(args);
                var updateResult = updateLastWorkingItemCommand.Run();
                ex = updateResult.Exception;
                message = updateResult.Message;

                //TODO add case where selected item is last item -> run GetLastWorkingItemCommand then?
            }
            else
            {
                var lastItemCmdArgs = new GetLastWorkingItemCommandArgs(application);
                var lastItemCmd = new GetLastWorkingItemCommand(lastItemCmdArgs);
                var lastItemResult = lastItemCmd.Run();

                if (lastItemResult.Exception == null && lastItemResult.Value != null)
                {
                    var items = lv_Categories.SelectedItems;
                    var category = items.Count > 0 ? items[0].Text : "";
                    var subject = tb_Message.Text;

                    var args = new UpdateLastWorkingItemCommandArguments(
                        application, DateTime.Today, DateTime.Now, subject, category);
                    var updateLastWorkingItemCommand = new UpdateLastWorkingItemCommand(args);
                    var updateResult = updateLastWorkingItemCommand.Run();
                    ex = updateResult.Exception;
                    message = updateResult.Message;
                }
            }

            if (ex != null)
            {
                var error = $@"Something went terribly wrong!{Environment.NewLine}{$"Exception: {message}"}";
                MessageBox.Show(error, @"Desaster!", MessageBoxButtons.OK);
            }
        }

        private void DynamicCalendarItemsBuzzer_Load(object sender, EventArgs e)
        {
            InitializeListView();
        }
    }
}
