﻿using System;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Commands;
using MyOutlookTimeTrackingExtension.Helper;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public partial class WeeklyWorkingHoursButton : DynamicTaskPaneSubControlBase
    {

        private readonly string _buttonTextFormat;

        public WeeklyWorkingHoursButton()
        {
            InitializeComponent();
            
            _buttonTextFormat = "Weekly working hours: {0}{1}{2}";

            UpdateText(TimeSpan.Zero, 0);
        }
        
        private void btn_MainButton_Click(object sender, EventArgs e)
        {
            var endDate = TimeHelper.GetSelectedWeeksEndDateOrNow(Globals.ThisAddIn.Application);
            var commandResult = new GetCurrentWeeksWorkingHoursCommand(new CurrentWeekArguments(Globals.ThisAddIn.Application, endDate)).Run();

            var averageHours = commandResult.Value.TotalHours / GetNumberOfWorkingDays(endDate);

            UpdateText(commandResult.Value, averageHours);
        }

        private static int GetNumberOfWorkingDays(DateTime endDate)
        {
            return (endDate.Date - TimeHelper.GetStartOfWeek(endDate).Date).Days + 1;
        }

        private void UpdateText(TimeSpan time, double averageHours)
        {
            var hours = (int)(time.TotalMinutes / 60);
            var mins = (int)(time.TotalMinutes % 60);
            UpdateText($"{hours}h {mins}m", $" ({Math.Round(averageHours, 2)}h)");
        }

        private void UpdateText(string totalString, string averageString)
        {
            btn_MainButton.Text = CreateButtonText(totalString, averageString);
        }

        private string CreateButtonText(string totalString, string averageString)
        {
            return string.Format(_buttonTextFormat, Environment.NewLine, totalString, averageString);
        }
    }
}
