namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public class WorkingItemCategory
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }

        public string Color { get; set; }

        public WorkingItemCategory(string id, string displayText, string color)
        {
            Id = id;
            DisplayText = displayText;
            Color = color;
        }
    }
}