﻿namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    partial class WeeklyWorkingHoursButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_MainButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_MainButton
            // 
            this.btn_MainButton.AutoSize = true;
            this.btn_MainButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_MainButton.Location = new System.Drawing.Point(0, 0);
            this.btn_MainButton.Name = "btn_MainButton";
            this.btn_MainButton.Size = new System.Drawing.Size(180, 100);
            this.btn_MainButton.TabIndex = 0;
            this.btn_MainButton.Text = "Click to refresh!";
            this.btn_MainButton.UseVisualStyleBackColor = true;
            this.btn_MainButton.Click += new System.EventHandler(this.btn_MainButton_Click);
            // 
            // WeeklyWorkingHoursButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_MainButton);
            this.Name = "WeeklyWorkingHoursButton";
            this.Size = new System.Drawing.Size(180, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_MainButton;
    }
}
