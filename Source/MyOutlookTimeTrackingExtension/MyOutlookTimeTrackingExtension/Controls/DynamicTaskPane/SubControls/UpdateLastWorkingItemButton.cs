﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Commands;
using MyOutlookTimeTrackingExtension.Helper;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public partial class UpdateLastWorkingItemButton : DynamicTaskPaneSubControlBase
    {
        private readonly string _buttonTextFormat;

        public UpdateLastWorkingItemButton()
        {
            InitializeComponent();
            
            _buttonTextFormat = "Update last item end time";
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            try
            {
                var startDate = TimeHelper.GetStartOfWeek(DateTime.Now);
                var endDate = TimeHelper.GetSelectedWeeksEndDateOrNow(Globals.ThisAddIn.Application);
                var commandResult = new UpdateLastWorkingItemEndTimeCommand(
                        new UpdateLastWorkingItemEndTimeCommandArguments(Globals.ThisAddIn.Application, startDate,
                            endDate))
                    .Run();

                if (commandResult.Exception != null)
                {
                    UpdateText(commandResult.Exception.Message);
                }
                else
                {
                    UpdateText(_buttonTextFormat);
                }
            }
            catch (Exception ex)
            {
                UpdateText($"Unexpected Error (Code: {ex.HResult})");
            }
        }


        private void UpdateText(string text)
        {
            btn_Update.Text = text;
        }

    }
}
