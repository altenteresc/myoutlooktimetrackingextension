﻿using System;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Commands;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public partial class SelectedItemsWorkingHoursButton : DynamicTaskPaneSubControlBase
    {
        private readonly string _buttonTextFormat;

        public SelectedItemsWorkingHoursButton()
        {
            InitializeComponent();

            _buttonTextFormat = "Selected items hours: {0}{1}{2}";

            UpdateButtonText(TimeSpan.Zero);
        }
        
        private void btn_MainButton_Click(object sender, EventArgs e)
        {
            var commandResult = new GetTotalHoursOfSelectedApointmentItemsCommand(new CommandArgument(Globals.ThisAddIn.Application)).Run();
            UpdateButtonText(commandResult.Value);
        }

        private void UpdateButtonText(TimeSpan time)
        {
            var hours = (int)(time.TotalMinutes / 60);
            var mins = (int)(time.TotalMinutes % 60);
            UpdateButtonText($"{hours}h {mins}m", $" ({Math.Round(time.TotalHours, 2)}h)");
        }

        private void UpdateButtonText(string niceFormatString, string decimalString)
        {
            btn_MainButton.Text = CreateButtonText(niceFormatString, decimalString);
        }

        private string CreateButtonText(string niceFormatString, string decimalString)
        {
            return string.Format(_buttonTextFormat, Environment.NewLine, niceFormatString, decimalString);
        }
    }
}
