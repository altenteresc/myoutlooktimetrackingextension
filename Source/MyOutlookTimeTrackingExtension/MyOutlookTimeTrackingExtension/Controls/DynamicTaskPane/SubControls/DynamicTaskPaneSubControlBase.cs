﻿using System.Windows.Forms;

namespace MyOutlookTimeTrackingExtension.Controls.DynamicTaskPane.SubControls
{
    public class DynamicTaskPaneSubControlBase : UserControl, IDynamicTaskPaneSubControl
    {
        public string SubControlName => nameof(GetType);
    }
}