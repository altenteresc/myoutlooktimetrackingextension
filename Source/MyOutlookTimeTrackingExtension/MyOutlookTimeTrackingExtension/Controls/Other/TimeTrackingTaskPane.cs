﻿using System;
using System.Windows.Forms;
using MyOutlookTimeTrackingExtension.Commands;
using MyOutlookTimeTrackingExtension.Helper;

//using Application = Microsoft.Office.Interop.Outlook.Application;

namespace MyOutlookTimeTrackingExtension.Controls.Other
{
    public partial class TimeTrackingTaskPane : UserControl
    {
        public TimeTrackingTaskPane()
        {
            InitializeComponent();
        }
        
        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            var application = Globals.ThisAddIn.Application;
            
            var selectedHoursResult = new GetTotalHoursOfSelectedApointmentItemsCommand(new CommandArgument(application)).Run();
            lbl_NumberOfHoursSelectedItems.Text = selectedHoursResult.Message;
        }

        private void btn_Refresh_TotalWeeklyHours_Click(object sender, EventArgs e)
        {
            var application = Globals.ThisAddIn.Application;

            var endDate = TimeHelper.GetSelectedWeeksEndDateOrNow(application);
            var currentWeeksWorkingHoursResult = new GetCurrentWeeksWorkingHoursCommand(new CurrentWeekArguments(application, endDate)).Run();
            lbl_NumberOfWorkingHours.Text = currentWeeksWorkingHoursResult.Message;
        }

    }
}
