﻿namespace MyOutlookTimeTrackingExtension.Controls.Other
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class TimeTrackingFormRegion : Microsoft.Office.Tools.Outlook.FormRegionBase
    {
        public TimeTrackingFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory, formRegion)
        {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.lbl_NumberOfWorkingHoursCurrentWeek = new System.Windows.Forms.Label();
            this.lbl_HoursCurrentWeek = new System.Windows.Forms.Label();
            this.lbl_NumberOfHoursCurrentItem = new System.Windows.Forms.Label();
            this.lbl_HoursCurrentItem = new System.Windows.Forms.Label();
            this.btn_RefreshCurrentElementHours = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_Refresh.Location = new System.Drawing.Point(233, 45);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(40, 24);
            this.btn_Refresh.TabIndex = 0;
            this.btn_Refresh.Text = "Akt.";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // lbl_NumberOfWorkingHoursCurrentWeek
            // 
            this.lbl_NumberOfWorkingHoursCurrentWeek.AutoSize = true;
            this.lbl_NumberOfWorkingHoursCurrentWeek.Location = new System.Drawing.Point(190, 51);
            this.lbl_NumberOfWorkingHoursCurrentWeek.Name = "lbl_NumberOfWorkingHoursCurrentWeek";
            this.lbl_NumberOfWorkingHoursCurrentWeek.Size = new System.Drawing.Size(19, 13);
            this.lbl_NumberOfWorkingHoursCurrentWeek.TabIndex = 3;
            this.lbl_NumberOfWorkingHoursCurrentWeek.Text = "0h";
            // 
            // lbl_HoursCurrentWeek
            // 
            this.lbl_HoursCurrentWeek.AutoSize = true;
            this.lbl_HoursCurrentWeek.Location = new System.Drawing.Point(3, 51);
            this.lbl_HoursCurrentWeek.Name = "lbl_HoursCurrentWeek";
            this.lbl_HoursCurrentWeek.Size = new System.Drawing.Size(124, 13);
            this.lbl_HoursCurrentWeek.TabIndex = 2;
            this.lbl_HoursCurrentWeek.Text = "Anzahl Wochenstunden:";
            // 
            // lbl_NumberOfHoursCurrentItem
            // 
            this.lbl_NumberOfHoursCurrentItem.AutoSize = true;
            this.lbl_NumberOfHoursCurrentItem.Location = new System.Drawing.Point(190, 12);
            this.lbl_NumberOfHoursCurrentItem.Name = "lbl_NumberOfHoursCurrentItem";
            this.lbl_NumberOfHoursCurrentItem.Size = new System.Drawing.Size(19, 13);
            this.lbl_NumberOfHoursCurrentItem.TabIndex = 5;
            this.lbl_NumberOfHoursCurrentItem.Text = "0h";
            // 
            // lbl_HoursCurrentItem
            // 
            this.lbl_HoursCurrentItem.AutoSize = true;
            this.lbl_HoursCurrentItem.Location = new System.Drawing.Point(3, 12);
            this.lbl_HoursCurrentItem.Name = "lbl_HoursCurrentItem";
            this.lbl_HoursCurrentItem.Size = new System.Drawing.Size(185, 13);
            this.lbl_HoursCurrentItem.TabIndex = 4;
            this.lbl_HoursCurrentItem.Text = "Anzahl Stunden in aktuellem Element:";
            // 
            // btn_RefreshCurrentElementHours
            // 
            this.btn_RefreshCurrentElementHours.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_RefreshCurrentElementHours.Location = new System.Drawing.Point(233, 6);
            this.btn_RefreshCurrentElementHours.Name = "btn_RefreshCurrentElementHours";
            this.btn_RefreshCurrentElementHours.Size = new System.Drawing.Size(40, 24);
            this.btn_RefreshCurrentElementHours.TabIndex = 6;
            this.btn_RefreshCurrentElementHours.Text = "Akt.";
            this.btn_RefreshCurrentElementHours.UseVisualStyleBackColor = true;
            this.btn_RefreshCurrentElementHours.Click += new System.EventHandler(this.btn_RefreshCurrentElementHours_Click);
            // 
            // TimeTrackingFormRegion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_RefreshCurrentElementHours);
            this.Controls.Add(this.lbl_NumberOfHoursCurrentItem);
            this.Controls.Add(this.lbl_HoursCurrentItem);
            this.Controls.Add(this.lbl_NumberOfWorkingHoursCurrentWeek);
            this.Controls.Add(this.lbl_HoursCurrentWeek);
            this.Controls.Add(this.btn_Refresh);
            this.Name = "TimeTrackingFormRegion";
            this.Size = new System.Drawing.Size(276, 79);
            this.FormRegionShowing += new System.EventHandler(this.TimeTrackingFormRegion_FormRegionShowing);
            this.FormRegionClosed += new System.EventHandler(this.TimeTrackingFormRegion_FormRegionClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest, Microsoft.Office.Tools.Outlook.Factory factory)
        {
            manifest.FormRegionName = "MainFormRegion";
            manifest.FormRegionType = Microsoft.Office.Tools.Outlook.FormRegionType.Adjoining;

        }

        #endregion

        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.Label lbl_NumberOfWorkingHoursCurrentWeek;
        private System.Windows.Forms.Label lbl_HoursCurrentWeek;
        private System.Windows.Forms.Label lbl_NumberOfHoursCurrentItem;
        private System.Windows.Forms.Label lbl_HoursCurrentItem;
        private System.Windows.Forms.Button btn_RefreshCurrentElementHours;

        public partial class TimeTrackingFormRegionFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory
        {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public TimeTrackingFormRegionFactory()
            {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                TimeTrackingFormRegion.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.TimeTrackingFormRegionFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest
            {
                get
                {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            {
                TimeTrackingFormRegion form = new TimeTrackingFormRegion(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                if (this.FormRegionInitializing != null)
                {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else
                {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind
            {
                get
                {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }
}
