﻿using System;
using MyOutlookTimeTrackingExtension.Commands;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MyOutlookTimeTrackingExtension.Controls.Other
{
    partial class TimeTrackingFormRegion
    {
        #region Form Region Factory 

        [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Appointment)]
        [Microsoft.Office.Tools.Outlook.FormRegionName("MyOutlookTimeTrackingExtension.TimeTrackingFormRegion")]
        public partial class TimeTrackingFormRegionFactory
        {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void TimeTrackingFormRegionFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
            {
                if (e.OutlookItem is Outlook.AppointmentItem)
                {

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        // Occurs before the form region is displayed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void TimeTrackingFormRegion_FormRegionShowing(object sender, System.EventArgs e)
        {
            CalculateCurrentItemsTotalHours();
        }

        // Occurs when the form region is closed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void TimeTrackingFormRegion_FormRegionClosed(object sender, System.EventArgs e)
        {
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            CalculateCurrentItemsTotalHours();
            CalculateCurrentWeeksWorkingHours();
        }


        private void CalculateCurrentItemsTotalHours()
        {
            if (this.OutlookItem is Outlook.AppointmentItem)
            {
                Outlook.AppointmentItem appointment = (Outlook.AppointmentItem)this.OutlookItem;

                var timespan = appointment.End - appointment.Start;
                lbl_NumberOfHoursCurrentItem.Text = Math.Round(timespan.TotalHours, 2) + @"h";
            }
        }

        private void CalculateCurrentWeeksWorkingHours()
        {
            var result = new GetCurrentWeeksWorkingHoursCommand(new CurrentWeekArguments(Globals.ThisAddIn.Application, DateTime.Now)).Run();
            lbl_NumberOfWorkingHoursCurrentWeek.Text = result.Message;
        }

        private void btn_RefreshCurrentElementHours_Click(object sender, EventArgs e)
        {
            CalculateCurrentItemsTotalHours();
        }
    }
}
